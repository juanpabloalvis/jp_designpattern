package com.patterns._01creational._03singleton;

public class ObjetoSingleton {
	private static ObjetoSingleton instance = new ObjetoSingleton();
	
	//hacemos el constructor del objeto privado, asi que no se puede instanciar.
	private ObjetoSingleton() {
	}
	
	//Obtener un único objeto disponible
	public static ObjetoSingleton getInstance(){
		return instance;
	}
	
	public void mostrarMensaje(){
		System.out.println("Hola mundo");
	}
	
}
