package com.patterns._01creational._03singleton;

public class DemostracionSingleton {

	public static void main(String[] args){
		//Error de compilacion, ya que el constructor es privado
		//ObjetoSingleton objetoSingleton = new ObjetoSingleton();
		
		//Obtener el unico objeto disponible,
		ObjetoSingleton objetoSingleton = ObjetoSingleton.getInstance();
		objetoSingleton.mostrarMensaje();
	}
}
