package com.patterns._01creational._02abstract_factory;

// PASO 6
public class FabricaFormas extends FabricaAbstracta{

	
	public FabricaFormas() {
		// TODO Auto-generated constructor stub
	}

	
	@Override
	Color getColor(ColorType color) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	Forma getForma(FormasType tipoForma) {
		if (tipoForma == null) {
			return null;
		}
		if (tipoForma.equals(FormasType.CIRCULO)) {
			return new Circulo();

		} else if (tipoForma.equals(FormasType.RECTANGULO)) {
			return new Rectangulo();

		} else if (tipoForma.equals(FormasType.CUADRADO)) {
			return new Cuadrado();
		}

		return null;
	}

}
