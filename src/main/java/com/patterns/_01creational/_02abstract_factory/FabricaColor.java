package com.patterns._01creational._02abstract_factory;

// PASO 6
public class FabricaColor extends FabricaAbstracta{

	
	public FabricaColor() {
		// TODO Auto-generated constructor stub
	}

	
	@Override
	Color getColor(ColorType color) {
		if (color == null) {
			return null;
		}
		if (color.equals(ColorType.AZUL)) {
			return new Azul();

		} else if (color.equals(ColorType.ROJO)) {
			return new Rojo();

		} else if (color.equals(ColorType.VERDE)) {
			return new Verde();
		}

		return null;
	}

	@Override
	Forma getForma(FormasType tipoForma) {
		return null;
	}

}
