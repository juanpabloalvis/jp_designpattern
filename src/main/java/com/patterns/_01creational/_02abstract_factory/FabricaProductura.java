package com.patterns._01creational._02abstract_factory;

// PASO 7.0
public class FabricaProductura {
	public static FabricaAbstracta getFabrica(FabricaType eleccion){
		if(eleccion.equals(FabricaType.FORMA)){
	         return new FabricaFormas();
	         
	      }else if(eleccion.equals(FabricaType.COLOR)){
	         return new FabricaColor();
	      }
	      
	      return null;
	}
}
