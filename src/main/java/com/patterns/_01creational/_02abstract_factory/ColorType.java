package com.patterns._01creational._02abstract_factory;

// paso 5.1
public enum ColorType {
	AZUL, VERDE, ROJO;
}
