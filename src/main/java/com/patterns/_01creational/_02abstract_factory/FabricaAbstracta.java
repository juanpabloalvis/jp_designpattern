package com.patterns._01creational._02abstract_factory;

// PASO 5
public abstract class FabricaAbstracta {
	abstract Color getColor(ColorType tipoColor);
	abstract Forma getForma(FormasType tipoForma) ;
}
