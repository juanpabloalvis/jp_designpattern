package com.patterns._01creational._02abstract_factory;

public class DemostracionAbstractFactoryPattern {
	public static void main(String args[]) {
		
		FabricaAbstracta fabricaFormas = FabricaProductura.getFabrica(FabricaType.FORMA);
		
		Forma forma1 = fabricaFormas.getForma(FormasType.CIRCULO);
		forma1.dibujar();
		Forma forma2 = fabricaFormas.getForma(FormasType.RECTANGULO);
		forma2.dibujar();
		Forma forma3 = fabricaFormas.getForma(FormasType.CUADRADO);
		forma3.dibujar();
		
		FabricaAbstracta fabricaColor = FabricaProductura.getFabrica(FabricaType.COLOR);
		
		Color color1 = fabricaColor.getColor(ColorType.AZUL);
		color1.llenar();
		Color color2 = fabricaColor.getColor(ColorType.ROJO);
		color2.llenar();
		Color color3 = fabricaColor.getColor(ColorType.VERDE);
		color3.llenar();
	}

}
