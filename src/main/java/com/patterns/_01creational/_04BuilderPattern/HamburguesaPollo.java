package com.patterns._01creational._04BuilderPattern;

import java.math.BigDecimal;

//Paso 4. Objetos concretos de que extienden de IHamburguesa e IBebidaFria
public class HamburguesaPollo extends AbstractHamburguesa {

	@Override
	public String nombre() {
		return "Hamburguesa Pollo";
	}

	@Override
	public BigDecimal getPrecio() {
		return new BigDecimal("21.000");
	}

}
