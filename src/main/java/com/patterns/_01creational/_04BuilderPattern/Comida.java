package com.patterns._01creational._04BuilderPattern;

import java.math.BigDecimal;
import java.util.ArrayList;

// Paso 5. Clase concreta "comida" que tiene objetos de tipo "Artículo" que definimos enel paso 4.
public class Comida {
	private ArrayList<IArticulo> articulos = new ArrayList<>();
	public void agregarArticulo(IArticulo articulo){
		articulos.add(articulo);
	}
	
	public BigDecimal getCostoTotal(){
		BigDecimal costoTotal =new BigDecimal("0.0");
		for (IArticulo iArticulo : articulos) {
			costoTotal = costoTotal.add(iArticulo.getPrecio());
		}
		return costoTotal;
	}
	
	public void mostrarArticulos(){
		for (IArticulo iArticulo : articulos) {
			System.out.print("Articulo: "+iArticulo.nombre());
			System.out.print(", empaquetado: "+iArticulo.empaquetado().empaquetar());
			System.out.println(", precio: "+iArticulo.getPrecio());
		}
	}

}
