package com.patterns._01creational._04BuilderPattern;

public class DemostracionBuilderPattern {

	public static void main(String[] args){
		BuilderComida builderComida = new BuilderComida(); 
		Comida vegetariana = builderComida.prepararComidaVegetariana();
		System.out.println("Comida vegetariana:");
		vegetariana.mostrarArticulos();
		System.out.println("Costo total: "+vegetariana.getCostoTotal());
		
		
		Comida noVegetariana = builderComida.prepararComidaNoVegetariana();
		System.out.println("Comida No Vegetariana:");
		noVegetariana.mostrarArticulos();
		System.out.println("Costo total: "+noVegetariana.getCostoTotal());
		
		
	}
}
