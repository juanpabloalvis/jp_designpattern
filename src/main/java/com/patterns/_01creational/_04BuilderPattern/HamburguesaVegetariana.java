package com.patterns._01creational._04BuilderPattern;

import java.math.BigDecimal;

//Paso 4. Objetos concretos de que extienden de IHamburguesa e IBebidaFria
public class HamburguesaVegetariana extends AbstractHamburguesa {

	@Override
	public String nombre() {
		return "Hamburguesa Vegetariana";
	}

	@Override
	public BigDecimal getPrecio() {
		return new BigDecimal("18.000");
	}

}
