package com.patterns._01creational._04BuilderPattern;
//Paso 6. La clase constructora actual es responsable  de crear objetos "Comida"
public class BuilderComida {
	
	public Comida prepararComidaVegetariana(){
		Comida comida = new Comida();
		comida.agregarArticulo(new HamburguesaVegetariana());
		comida.agregarArticulo(new BigCola());
		return comida;
	}
	
	public Comida prepararComidaNoVegetariana(){
		Comida comida = new Comida();
		comida.agregarArticulo(new HamburguesaPollo());
		comida.agregarArticulo(new Cocacola());
		return comida;
	}
}
