package com.patterns._01creational._04BuilderPattern;

import java.math.BigDecimal;

// Paso 1
public interface IArticulo {

	public String nombre();
	public IEmpaquetado empaquetado();
	public BigDecimal getPrecio();
	
}
