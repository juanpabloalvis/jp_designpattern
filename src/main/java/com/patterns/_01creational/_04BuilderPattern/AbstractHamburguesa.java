package com.patterns._01creational._04BuilderPattern;

import java.math.BigDecimal;

// Paso 3. Clase abstracta que implementa la interface "articulo" brindando funcionalidades por defecto

public abstract class AbstractHamburguesa implements IArticulo{

	@Override
	public IEmpaquetado empaquetado() {
		// TODO Auto-generated method stub
		return new Envoltorio();
	}
	
	@Override
	public abstract BigDecimal getPrecio();
}
