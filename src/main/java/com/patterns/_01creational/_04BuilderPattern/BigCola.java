package com.patterns._01creational._04BuilderPattern;

import java.math.BigDecimal;

// Paso 4. Objetos concretos de que extienden de IHamburguesa e IBebidaFria
public class BigCola extends AbstractBebidaFria {

	@Override
	public String nombre() {
		return "BigCola";
	}

	@Override
	public BigDecimal getPrecio() {
		return new BigDecimal("3.000");
	}

}
