package com.patterns._01creational._01factory;

public enum FormasType {
	CIRCULO, CUADRADO, RECTANGULO;
}
