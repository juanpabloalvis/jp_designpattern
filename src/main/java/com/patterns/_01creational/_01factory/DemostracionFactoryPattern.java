package com.patterns._01creational._01factory;

// Paso 4, demostracion
public class DemostracionFactoryPattern {
	public static void main(String args[]) {
		FabricaFormas fabricaDeFormas = new FabricaFormas();
		Forma forma1 = fabricaDeFormas.getFormas(FormasType.CIRCULO);
		forma1.dibujar();
		Forma forma2 = fabricaDeFormas.getFormas(FormasType.RECTANGULO);
		forma2.dibujar();
		Forma forma3 = fabricaDeFormas.getFormas(FormasType.CUADRADO);
		forma3.dibujar();
	}

}
