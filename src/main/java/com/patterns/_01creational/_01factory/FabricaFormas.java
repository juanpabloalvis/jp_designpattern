package com.patterns._01creational._01factory;

// PASO 3
public class FabricaFormas {

	
	public FabricaFormas() {
		// TODO Auto-generated constructor stub
	}

	public Forma getFormas(FormasType tipoForma) {
		if (tipoForma == null) {
			return null;
		}
		if (tipoForma.equals(FormasType.CIRCULO)) {
			return new Circulo();

		} else if (tipoForma.equals(FormasType.RECTANGULO)) {
			return new Rectangulo();

		} else if (tipoForma.equals(FormasType.CUADRADO)) {
			return new Cuadrado();
		}

		return null;
	}

}
