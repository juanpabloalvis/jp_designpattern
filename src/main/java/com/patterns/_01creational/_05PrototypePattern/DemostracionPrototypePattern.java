package com.patterns._01creational._05PrototypePattern;

public class DemostracionPrototypePattern {

	public static void main(String args[]){
		FormaCache.cargarCache();

		AbstractForma formaClonada = FormaCache.getForma("1");
		System.out.println("Forma: "+formaClonada.getTipo().toString());
		
		AbstractForma formaClonada2 = FormaCache.getForma("2");
		System.out.println("Forma: "+formaClonada2.getTipo().toString());
		
		AbstractForma formaClonada3 = FormaCache.getForma("3");
		System.out.println("Forma: "+formaClonada3.getTipo().toString());
		
	}
}
