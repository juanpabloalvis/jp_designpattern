package com.patterns._01creational._05PrototypePattern;

public enum Forma {
	RECTANGULO, CUADRADO, CIRCULO
}
