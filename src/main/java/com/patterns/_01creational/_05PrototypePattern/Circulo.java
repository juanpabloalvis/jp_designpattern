package com.patterns._01creational._05PrototypePattern;

// Paso 2. Clases concretas que extienden de "FormaAbstracta"
public class Circulo extends AbstractForma{

	public Circulo() {
		tipo = Forma.CIRCULO;
	}
	@Override
	void dibujar() {
		System.out.printf("\nDentro del metodo %s de la clase %s.",new Object(){}.getClass().getEnclosingMethod().getName(), this.getClass().getSimpleName());
		
	}
	

}
