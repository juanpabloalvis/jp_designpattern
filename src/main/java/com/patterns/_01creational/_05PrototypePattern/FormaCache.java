package com.patterns._01creational._05PrototypePattern;

import java.util.Hashtable;

// Paso 3. Creamos clase concreta para obtener clases concretas desde una
// base de datos y almacenarlas en una "HashTable"
public class FormaCache {
	private static Hashtable<String, AbstractForma> formaMap = new Hashtable<String, AbstractForma>();

	public static AbstractForma getForma(String id) {
		AbstractForma cacheForma = formaMap.get(id);
		return (AbstractForma) cacheForma.clone();
	}
	
	//Para cada forma ejecutamos una consulta en la base de datos y
	// creamos una "Forma"
	// Por ejemplo, añadimos tres formas
	public static void cargarCache(){
		Circulo circulo = new Circulo();
		circulo.setId("1");
		
		Cuadrado cuadrado = new Cuadrado();
		cuadrado.setId("2");
		
		Rectangulo rectangulo = new Rectangulo();
		rectangulo.setId("3");

		formaMap.put(circulo.getId(), circulo);
		formaMap.put(cuadrado.getId(), cuadrado);
		formaMap.put(rectangulo.getId(), rectangulo);
	}
	
	

}
