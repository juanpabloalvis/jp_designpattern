package com.patterns._01creational._05PrototypePattern;

// Paso 1. 
public abstract class AbstractForma implements Cloneable{
	private String id;
	protected Forma tipo;
	
	abstract void dibujar();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Forma getTipo() {
		return tipo;
	}
	public void setTipo(Forma tipo) {
		this.tipo = tipo;
	}

	public Object clone(){
		Object clone  = null;
		try {
			clone = super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return clone;
	}
	
}
