package com.patterns._03behavioral._20ObserverPattern;

public class DemostracionObserverPattern {

	public static void main(String[] args) {
		Sujeto sujeto = new Sujeto();
		
		new ObservadorBinario(sujeto);
		new ObservadorOctal(sujeto);
		new ObservadorHexadecimal(sujeto);

		System.out.println("Primer cambio de estado 18");
		sujeto.setEstado(18);
		System.out.println("Segundo cambio de estado 100");
		sujeto.setEstado(100);
	}

}
