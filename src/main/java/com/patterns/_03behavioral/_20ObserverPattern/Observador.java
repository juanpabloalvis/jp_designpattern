package com.patterns._03behavioral._20ObserverPattern;

//Paso 2. Clase observador.
public abstract class Observador {
	protected Sujeto sujeto;
	public abstract void actualizar();
}
