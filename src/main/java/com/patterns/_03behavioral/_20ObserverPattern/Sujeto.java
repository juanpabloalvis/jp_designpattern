package com.patterns._03behavioral._20ObserverPattern;

import java.util.ArrayList;
import java.util.List;

//Paso 1. El patron observer es utilizado cuando hay una relación uno a
// muchos entre objetos, tal que cuando un objeto sea modificado, sus
// objetos dependientes, son notificados automáticamente. 
public class Sujeto {
	private List<Observador> observadores = new ArrayList<>();
	private int estado;
	
	public int getEstado(){
		return estado;
	}
	
	public void setEstado(int estado){
		this.estado = estado;
		notifiqueTodosLosObservadores();
	}

	public void agregar(Observador observador){
		observadores.add(observador);
	}
	private void notifiqueTodosLosObservadores() {
		for (Observador observador : observadores) {
			observador.actualizar();
		}
	}
	
	
}
