package com.patterns._03behavioral._20ObserverPattern;

public class ObservadorHexadecimal extends Observador {

	public ObservadorHexadecimal(Sujeto sujeto) {
		this.sujeto = sujeto;
		this.sujeto.agregar(this);
	}
	
	@Override
	public void actualizar() {
		System.out.println("Cadena hexadecimal: "+Integer.toHexString(sujeto.getEstado()));
	}

}
