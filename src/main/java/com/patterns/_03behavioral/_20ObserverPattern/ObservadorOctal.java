package com.patterns._03behavioral._20ObserverPattern;

public class ObservadorOctal extends Observador {

	public ObservadorOctal(Sujeto sujeto) {
		this.sujeto = sujeto;
		this.sujeto.agregar(this);
	}
	
	@Override
	public void actualizar() {
		System.out.println("Cadena octal: "+Integer.toOctalString(sujeto.getEstado()));
	}

}
