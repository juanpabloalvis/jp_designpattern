package com.patterns._03behavioral._20ObserverPattern;

public class ObservadorBinario extends Observador {

	public ObservadorBinario(Sujeto sujeto) {
		this.sujeto = sujeto;
		this.sujeto.agregar(this);
	}
	
	@Override
	public void actualizar() {
		System.out.println("Cadena binaria: "+Integer.toBinaryString(sujeto.getEstado()));
	}

}
