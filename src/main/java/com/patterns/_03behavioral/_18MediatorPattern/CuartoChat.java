package com.patterns._03behavioral._18MediatorPattern;

import java.util.Date;

//Paso 1. Creamos clase mediador
public class CuartoChat {
	public static void mostrarMensaje(Usuario usuario, String mensaje){
		System.out.println(new Date().toString()+" ["+usuario.getNombre()+"]: "+mensaje);
	}

}
