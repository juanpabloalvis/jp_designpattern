package com.patterns._03behavioral._18MediatorPattern;

public class DemostracionMediatorPattern {

	public static void main(String[] args) {
		Usuario kiko = new Usuario("Kiko");
		Usuario donRamón = new Usuario("Don Ramón");
		
		kiko.enviarMensaje("Chusma, Chusma Trrrrru");		
		donRamón.enviarMensaje("Si serás, si serás");		
		donRamón.enviarMensaje("¿Qué pasó, qué pasó, vamos ay?");

	}

}
