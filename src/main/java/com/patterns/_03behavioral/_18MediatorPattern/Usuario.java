package com.patterns._03behavioral._18MediatorPattern;

//Paso 2. Creamos clase usuario.
public class Usuario {
	private String nombre;

	public Usuario(String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void enviarMensaje(String mensaje) {
		CuartoChat.mostrarMensaje(this, mensaje);
	}

}
