package com.patterns._03behavioral._16InterpreterPattern;

//Paso 2. Clases concretas que implementan la interface expresión.
public class ExpresionOr implements Expresion {
	private Expresion expresion1 = null;
	private Expresion expresion2 = null;
	

	public ExpresionOr(Expresion expresion1, Expresion expresion2) {
		this.expresion1 = expresion1;
		this.expresion2 = expresion2;
	}

	@Override
	public boolean interpretar(String contexto) {
		return expresion1.interpretar(contexto) || expresion2.interpretar(contexto);
	}

}
