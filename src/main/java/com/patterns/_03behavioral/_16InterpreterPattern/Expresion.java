package com.patterns._03behavioral._16InterpreterPattern;

//Paso 1. Creamos interface expresión
public interface Expresion {
	public boolean interpretar(String contexto);
}
