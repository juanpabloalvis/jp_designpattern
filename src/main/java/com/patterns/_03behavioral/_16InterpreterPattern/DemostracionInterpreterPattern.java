package com.patterns._03behavioral._16InterpreterPattern;

public class DemostracionInterpreterPattern {

	// Rule: Robert and John are male
	public static Expresion getMaleExpression() {
		Expresion robert = new ExpresionTerminal("Robert");
		Expresion john = new ExpresionTerminal("John");
		return new ExpresionOr(robert, john);
	}

	// Rule: Julie is a married women
	public static Expresion getMarriedWomanExpression() {
		Expresion julie = new ExpresionTerminal("Julie");
		Expresion married = new ExpresionTerminal("Married");
		return new ExpresionAnd(julie, married);
	}

	public static void main(String[] args) {
		Expresion isMale = getMaleExpression();
		Expresion isMarriedWoman = getMarriedWomanExpression();

		System.out.println("John is male? " + isMale.interpretar("John"));
		System.out.println("Julie is a married women? " + isMarriedWoman.interpretar("Married Julie"));

	}

}
