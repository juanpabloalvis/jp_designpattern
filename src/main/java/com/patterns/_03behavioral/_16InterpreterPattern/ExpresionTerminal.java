package com.patterns._03behavioral._16InterpreterPattern;

//Paso 2. Clases concretas que implementan la interface expresión.
public class ExpresionTerminal implements Expresion {
	private String dato;

	public ExpresionTerminal(String dato) {
		this.dato = dato;
	}

	@Override
	public boolean interpretar(String contexto) {
		if (contexto.contains(dato)) {
			return true;
		}
		return false;
	}

}
