package com.patterns._03behavioral._15CommandPattern;

// Paso 2. Crear clase que hace solicitud.
public class Accion {
	
	public Accion(String nombre, Integer cantidad) {
		super();
		this.nombre = nombre;
		this.cantidad = cantidad;
	}

	private String nombre = "";
	private Integer cantidad = new Integer("0");
	
	public void comprar(){
		System.out.println("Acción [ Nombre: "+nombre+","
				+ "Cantidad: " + cantidad +" ] comprada.");
	}
	
	public void vender(){
		System.out.println("Acción [ Nombre: "+nombre+","
				+ "Cantidad: " + cantidad +" ] vendida.");
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
