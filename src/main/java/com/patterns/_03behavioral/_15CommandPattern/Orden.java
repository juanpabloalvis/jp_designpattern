package com.patterns._03behavioral._15CommandPattern;

//Paso 1. Crear una interface de comando.
public interface Orden {
	void ejecutar();
}
