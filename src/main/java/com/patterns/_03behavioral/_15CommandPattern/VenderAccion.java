package com.patterns._03behavioral._15CommandPattern;

 
public class VenderAccion implements Orden {

	private Accion accionExon;

	public VenderAccion(Accion accionExon) {
		this.accionExon = accionExon;
	}

	@Override
	public void ejecutar() {
		accionExon.vender();
	}

}
