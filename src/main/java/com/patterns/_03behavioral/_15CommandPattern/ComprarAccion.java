package com.patterns._03behavioral._15CommandPattern;

//Paso 3. Clase concreta que implementa òrden
public class ComprarAccion implements Orden {

	private Accion accionExon;

	public ComprarAccion(Accion accionExon) {
		this.accionExon = accionExon;
	}

	@Override
	public void ejecutar() {
		accionExon.comprar();
	}

}
