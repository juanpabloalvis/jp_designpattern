package com.patterns._03behavioral._15CommandPattern;

import java.util.ArrayList;
import java.util.List;

//Paso 4. Creando clase invocador.
public class Broker {
	private List<Orden> listaOrden = new ArrayList<Orden>(); 
	
	public void seguirOrden(Orden orden){
		listaOrden.add(orden);
	}
	
	public void colocarOrdenes(){
		for (Orden orden : listaOrden) {
			orden.ejecutar();
		}
		listaOrden.clear();
	}
	
	
}
