package com.patterns._03behavioral._15CommandPattern;

public class DemostracionCommandPattern {

	public static void main(String []args){
		Accion accionJPM = new Accion("JPM", 10000);
		Accion accionAAPL = new Accion("AAPL", 500);
		ComprarAccion ordenDeCompra = new ComprarAccion(accionJPM);
		VenderAccion ordenDeVenta = new VenderAccion(accionAAPL);
		
		Broker broker = new Broker();
		broker.seguirOrden(ordenDeCompra);
		broker.seguirOrden(ordenDeVenta);
		
		broker.colocarOrdenes();
	}
}
