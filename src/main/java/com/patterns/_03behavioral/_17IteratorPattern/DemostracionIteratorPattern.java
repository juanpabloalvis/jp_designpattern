package com.patterns._03behavioral._17IteratorPattern;

public class DemostracionIteratorPattern {

	public static void main(String[] args) {
		RepositorioNombre repositorioNombre = new RepositorioNombre();

	      for(Iterador iter = repositorioNombre.getIterador(); iter.tieneSiguiente();){
	         String name = (String)iter.siguiente();
	         System.out.println("Name : " + name);
	      } 	

	}

}
