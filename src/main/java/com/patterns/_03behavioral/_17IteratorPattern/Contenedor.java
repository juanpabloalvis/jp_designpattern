package com.patterns._03behavioral._17IteratorPattern;

//Paso 1. Creamos las interfaces
public interface Contenedor {
	public Iterador getIterador();
}
