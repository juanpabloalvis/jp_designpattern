package com.patterns._03behavioral._17IteratorPattern;

public class RepositorioNombre implements Contenedor {

	public String nombres[] = { "Robert", "John", "Julie", "Lora" };

	@Override
	public Iterador getIterador() {
		return new IteradorNombre();
	}

	private class IteradorNombre implements Iterador {
		int index;

		@Override
		public boolean tieneSiguiente() {
			if (index < nombres.length) {
				return true;
			}
			return false;
		}

		@Override
		public Object siguiente() {
			if (this.tieneSiguiente()) {
				return nombres[index++];
			}
			return null;
		}
	}

}
