package com.patterns._03behavioral._17IteratorPattern;

//Paso 1. Creamos las interfaces
public interface Iterador {
	public boolean tieneSiguiente();
	public Object siguiente();
}
