package com.patterns._03behavioral._14ChainOfResponsabilityPattern;


// Paso 1. Creamos una clase Logger abstracta.
public abstract class LoggerAbstracto {

	public static int INFO = 1;
	public static int DEBUG = 2;
	public static int ERROR = 1;

	protected int nivel;

	// proximo elemento en la cadena de responsabilidad.
	protected LoggerAbstracto siguienteLogger;

	public void setSiguienteLogger(LoggerAbstracto siguenteLogger) {
		this.siguienteLogger = siguenteLogger;
	}

	public void mensajeLog(int nivel, String mensaje) {
		if (this.nivel <= nivel) {
			escribirMensaje(mensaje);
		}
		if (siguienteLogger != null) {
			siguienteLogger.mensajeLog(nivel, mensaje);
		}
	}

	abstract protected void escribirMensaje(String mensaje);
}
