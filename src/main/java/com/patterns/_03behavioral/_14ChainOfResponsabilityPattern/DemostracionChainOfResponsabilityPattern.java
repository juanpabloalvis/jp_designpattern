package com.patterns._03behavioral._14ChainOfResponsabilityPattern;

public class DemostracionChainOfResponsabilityPattern {

	public static void main(String[] args) {
		LoggerAbstracto cadenaLoggers = getLoggerDeCadenaDeResponsabilidad();

		cadenaLoggers.mensajeLog(LoggerAbstracto.INFO, "Esto es una informacion.");

		cadenaLoggers.mensajeLog(LoggerAbstracto.DEBUG, "Esto es una inforamacion de nivel de debug.");

		cadenaLoggers.mensajeLog(LoggerAbstracto.ERROR, "Esto es una inforamacion de nivel de debug.");
	}

	private static LoggerAbstracto getLoggerDeCadenaDeResponsabilidad() {

		LoggerAbstracto errorLogger = new LoggerError(LoggerAbstracto.ERROR);
		LoggerAbstracto archivoLogger = new LoggerArchivo(LoggerAbstracto.DEBUG);
		LoggerAbstracto consolaLogger = new LoggerConsola(LoggerAbstracto.INFO);

		errorLogger.setSiguienteLogger(archivoLogger);
		archivoLogger.setSiguienteLogger(consolaLogger);

		return errorLogger;
	}
}
