package com.patterns._03behavioral._14ChainOfResponsabilityPattern;

//Paso 2. Clases concretas que extienden del logger
public class LoggerConsola extends LoggerAbstracto {
	
	public LoggerConsola(int nivel) {
		this.nivel = nivel;
	}

	@Override
	protected void escribirMensaje(String mensaje) {
		System.out.println("Consola estándar::logger"+mensaje);

	}

}
