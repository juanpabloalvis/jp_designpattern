package com.patterns._03behavioral._19MementoPattern;

//Paso 1. Creamos clase memento, o recuerdo.
public class Memento {

	private String estado;

	public Memento(String estado) {
		super();
		this.estado = estado;
	}

	public String getEstado() {
		return estado;
	}

}
