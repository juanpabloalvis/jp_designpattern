package com.patterns._03behavioral._19MementoPattern;

public class DemostracionMementoPattern {

	public static void main(String[] args) {
		 Originator originator = new Originator();
	      CareTaker careTaker = new CareTaker();
	      
	      originator.setEstado("Estado #1");
	      originator.setEstado("Estado #2");
	      careTaker.add(originator.guardarEstadoAMemento());
	      
	      originator.setEstado("Estado #3");
	      careTaker.add(originator.guardarEstadoAMemento());
	      
	      originator.setEstado("Estado #4");
	      System.out.println("Estado actual: " + originator.getEstado());		
	      
	      originator.getEstadoDesdeMemento(careTaker.get(0));
	      System.out.println("Primer estado guardado: " + originator.getEstado());
	      originator.getEstadoDesdeMemento(careTaker.get(1));
	      System.out.println("Segundo estado guardado: " + originator.getEstado());

	}

}
