package com.patterns._03behavioral._19MementoPattern;

//Paso 2. Clase Originador, o creador. 
public class Originator {
	private String estado;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public Memento guardarEstadoAMemento(){
		return new Memento(estado);
	}
	
	public void getEstadoDesdeMemento(Memento memento){
		estado = memento.getEstado();
	}
}
