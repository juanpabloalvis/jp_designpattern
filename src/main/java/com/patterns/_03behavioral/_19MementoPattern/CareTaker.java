package com.patterns._03behavioral._19MementoPattern;

import java.util.ArrayList;
import java.util.List;

//Paso 3. CareTaker, Vigilante, cuidador
public class CareTaker {
	private List<Memento> listaMemento = new ArrayList<Memento>();

	public void add(Memento estado) {
		listaMemento.add(estado);
	}

	public Memento get(int index) {
		return listaMemento.get(index);
	}
}
