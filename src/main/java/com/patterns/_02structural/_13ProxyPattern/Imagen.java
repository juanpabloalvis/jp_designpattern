package com.patterns._02structural._13ProxyPattern;

//Paso 1. Creamos la interface
public interface Imagen {
	void mostrar();
}
