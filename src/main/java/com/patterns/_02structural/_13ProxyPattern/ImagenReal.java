package com.patterns._02structural._13ProxyPattern;

//Paso 2.1. Creamos clase concreta que implementa la interface
public class ImagenReal implements Imagen {
	
	private String nombreArchivo;

	public ImagenReal(String nombreArchivo) {
		super();
		this.nombreArchivo = nombreArchivo;
		cargarDeDisco(nombreArchivo);
	}

	private void cargarDeDisco(String nombreArchivo) {
		System.out.println("Cargando "+nombreArchivo+"...");
	}

	@Override
	public void mostrar() {
		System.out.println("Mostrando "+nombreArchivo);
	}

}
