package com.patterns._02structural._13ProxyPattern;

//Paso 2.2.  
public class ImagenProxy implements Imagen {

	private ImagenReal imagenReal;
	private String nombreArchivo;
	
	public ImagenProxy(String nombreArchivo) {
		super();
		this.nombreArchivo = nombreArchivo;
	}

	@Override
	public void mostrar() {
		if(imagenReal==null){
			imagenReal = new ImagenReal(nombreArchivo);
		}
		imagenReal.mostrar();
	}

}
