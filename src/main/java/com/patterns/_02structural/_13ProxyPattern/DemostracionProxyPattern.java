package com.patterns._02structural._13ProxyPattern;

public class DemostracionProxyPattern {

	public static void main(String[] args) {
		Imagen imagen = new ImagenProxy("foto.jpg");
		//Imagen cargada desde disco:
		imagen.mostrar();
		System.out.println();
		//Imagen no será cargada desde disco:
		imagen.mostrar();
				
	}

}
