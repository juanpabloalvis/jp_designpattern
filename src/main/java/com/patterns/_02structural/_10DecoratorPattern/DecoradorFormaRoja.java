package com.patterns._02structural._10DecoratorPattern;

public class DecoradorFormaRoja extends DecoradorForma{

	public DecoradorFormaRoja(Forma formaDecorada) {
		super(formaDecorada);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void dibujar() {
		super.dibujar();
		setBordeRojo(formaDecorada);
	}

	private void setBordeRojo(Forma formaDecorada) {
		System.out.println("Borde color rojo");
		
	}

	


}
