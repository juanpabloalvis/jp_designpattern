package com.patterns._02structural._10DecoratorPattern;

// Demostracion: utiliza la clase "DecoradorFormaRoja" para decorar objetos tipo "Forma"
public class DemostracionDecoratorPattern {

	public static void main(String[] args){
		Forma circulo = new Circulo();
		Forma circuloRojo = new DecoradorFormaRoja(new Circulo());
		Forma rectanguloRojo = new DecoradorFormaRoja(new Circulo());
		
		System.out.println("Circulo normal: ");
		circulo.dibujar();
		
		System.out.println("Circulo con borde rojo: ");
		circuloRojo.dibujar();
		
		System.out.println("Rectangulo con borde rojo: ");
		rectanguloRojo.dibujar();		
		
	}
}
