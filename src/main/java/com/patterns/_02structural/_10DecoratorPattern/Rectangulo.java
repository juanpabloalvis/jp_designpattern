package com.patterns._02structural._10DecoratorPattern;

// Paso 2. Clases concretas implementando la misma interface.
public class Rectangulo implements Forma {

	@Override
	public void dibujar() {
		System.out.println("Forma: Rectangulo");
		
	}

}
