package com.patterns._02structural._10DecoratorPattern;

// Paso 3. Crea una clase decorador abstracta implementando la interface "Forma"
public abstract class DecoradorForma implements Forma {

	protected Forma formaDecorada;
	
	public DecoradorForma(Forma formaDecorada) {
		super();
		this.formaDecorada = formaDecorada;
	}

	@Override
	public void dibujar() {
		formaDecorada.dibujar();

	}
	
}
