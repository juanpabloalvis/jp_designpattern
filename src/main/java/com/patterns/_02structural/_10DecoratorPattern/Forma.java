package com.patterns._02structural._10DecoratorPattern;

// Paso 1. creamos la interface
public interface Forma {
	void dibujar();
}
