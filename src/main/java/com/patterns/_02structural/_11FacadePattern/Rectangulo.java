package com.patterns._02structural._11FacadePattern;

// Paso 2. Clases concretas implementando la misma interface.
public class Rectangulo implements Forma {

	@Override
	public void dibujar() {
		System.out.println("Rectangulo::Dibujar");
		
	}

}
