package com.patterns._02structural._11FacadePattern;

// Paso 1. creamos la interface
public interface Forma {
	void dibujar();
}
