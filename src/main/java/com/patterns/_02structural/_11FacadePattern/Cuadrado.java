package com.patterns._02structural._11FacadePattern;

// Paso 2. Clases concretas implementando la misma interface.
public class Cuadrado implements Forma {

	@Override
	public void dibujar() {
		System.out.println("Cuadrado::Dibujar");
		
	}

}
