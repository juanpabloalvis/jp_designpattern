package com.patterns._02structural._11FacadePattern;

public class DemostracionFacadePattern {
	public static void main (String args[]){
		FabricanteFormas fabricanteFormas = new FabricanteFormas();
		
		fabricanteFormas.dibujarCirculo();
		fabricanteFormas.dibujarCuadrado();
		fabricanteFormas.dibujarRectangulo();
		
	}
}
