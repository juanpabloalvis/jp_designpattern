package com.patterns._02structural._11FacadePattern;

public class FabricanteFormas {

	private Forma circulo;
	private Forma rectangulo;
	private Forma cuadrado;

	public FabricanteFormas() {
		super();
		circulo = new Circulo();
		rectangulo = new Rectangulo();
		cuadrado = new Cuadrado();
	}

	public void dibujarCirculo() {
		circulo.dibujar();
	}

	public void dibujarRectangulo() {
		rectangulo.dibujar();
	}

	public void dibujarCuadrado() {
		cuadrado.dibujar();
	}
}
