package com.patterns._02structural._08FilterPattern;

public enum EstadoCivilType {
	SOLTERO,
	CASADO,
	VIUDO,
	RELIGIOSO
}
