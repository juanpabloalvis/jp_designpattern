package com.patterns._02structural._08FilterPattern;

import java.util.List;

//Paso 2. Creamos una interface criterio
public interface Criterio {

	public List<Persona> buscarCriterio(List<Persona> personas);
}
