package com.patterns._02structural._08FilterPattern;

import java.util.List;

// Paso 3. Creamos clases concretas implementando el criterio
public class CriterioAnd implements Criterio {

	private Criterio criterio;
	private Criterio otroCriterio;
	
	
	
	public CriterioAnd(Criterio criterio, Criterio otroCriterio) {
		super();
		this.criterio = criterio;
		this.otroCriterio = otroCriterio;
	}



	@Override
	public List<Persona> buscarCriterio(List<Persona> personas) {
		// Retorna una lista de personas filtradas por el primer criterio
		List<Persona> otrosCriterios = criterio.buscarCriterio(personas);
		// aplico el filtlro del otrocritiro a la lista anterior.
		return otroCriterio.buscarCriterio(otrosCriterios);
	}

}
