package com.patterns._02structural._08FilterPattern;


// Paso 1. Creamos una clase persona.
public class Persona {

	private String nombre;
	private String apellido;
	private GeneroType genero;
	private EstadoCivilType estadoCivil;
	
	
	
	public Persona(String nombre, String apellido, GeneroType genero, EstadoCivilType estadoCivil) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.genero = genero;
		this.estadoCivil = estadoCivil;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public GeneroType getGenero() {
		return genero;
	}
	public void setGenero(GeneroType genero) {
		this.genero = genero;
	}
	public EstadoCivilType getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(EstadoCivilType estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	
	

}
