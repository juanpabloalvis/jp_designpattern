package com.patterns._02structural._08FilterPattern;

import java.util.ArrayList;
import java.util.List;

public class DemostracionFilterPattern {
	
	public static void main(String args[]){
	
		List<Persona> personas = new ArrayList<Persona>();

		personas.add(new Persona("Robert","Gomez", GeneroType.MASCULINO, EstadoCivilType.CASADO));
		personas.add(new Persona("John", "Travolta", GeneroType.MASCULINO, EstadoCivilType.CASADO));
		personas.add(new Persona("Florinda", "Mesa", GeneroType.FEMENINO, EstadoCivilType.SOLTERO));
		personas.add(new Persona("La", "Chilindrina", GeneroType.FEMENINO, EstadoCivilType.SOLTERO));
		personas.add(new Persona("Doña", "Cleotilde", GeneroType.FEMENINO, EstadoCivilType.SOLTERO));
		personas.add(new Persona("Don", "Ramón", GeneroType.MASCULINO, EstadoCivilType.VIUDO));
		personas.add(new Persona("Chavo", "del 8", GeneroType.MASCULINO, EstadoCivilType.SOLTERO));
		
		 Criterio male = new CriterioMasculio();
	     Criterio female = new CriterioFemenino();
	     Criterio single = new CriterioSoltero();
	     Criterio singleFemenino = new CriterioAnd(single, female);
	     Criterio singleOrFemale = new CriterioOr(single, female);
	     
	     System.out.println("Masculinos: ");
	     imprimirPersons(male.buscarCriterio(personas));

	      System.out.println("\nFemeninos: ");
	      imprimirPersons(female.buscarCriterio(personas));

	      System.out.println("\nSolteros y femeninos: ");
	      imprimirPersons(singleFemenino.buscarCriterio(personas));

	      System.out.println("\nSolteros O femeninos: ");
	      imprimirPersons(singleOrFemale.buscarCriterio(personas));
	}
	
	public static void imprimirPersons(List<Persona> persons){
		   
	      for (Persona person : persons) {
	         System.out.println("Persona : [ Nombre : " + person.getNombre() + ", Genero : " + person.getGenero() + ", Estado civil : " + person.getEstadoCivil() + " ]");
	      }
	   }   
}
