package com.patterns._02structural._08FilterPattern;

import java.util.List;

// Paso 3. Creamos clases concretas implementando el criterio
public class CriterioOr implements Criterio {

	private Criterio criterio;
	private Criterio otroCriterio;
	
	
	
	public CriterioOr(Criterio criterio, Criterio otroCriterio) {
		super();
		this.criterio = criterio;
		this.otroCriterio = otroCriterio;
	}

	@Override
	public List<Persona> buscarCriterio(List<Persona> personas) {
		List<Persona> primerosCriterios = criterio.buscarCriterio(personas);
		List<Persona> otrosCriterios = otroCriterio.buscarCriterio(personas);
		
		for (Persona persona : otrosCriterios) {
			if(!primerosCriterios.contains(persona)){
				primerosCriterios.add(persona);
			}			
		}		
		return primerosCriterios;
	}

}
