package com.patterns._02structural._08FilterPattern;

import java.util.ArrayList;
import java.util.List;

// Paso 3. Creamos clases concretas implementando el criterio
public class CriterioSoltero implements Criterio {

	@Override
	public List<Persona> buscarCriterio(List<Persona> personas) {
		List<Persona> personasMasculinas = new ArrayList<Persona>();
		
		for (Persona persona : personas) {
			if(persona.getEstadoCivil().equals(EstadoCivilType.SOLTERO))
				personasMasculinas.add(persona);
		}
		return personasMasculinas;
	}

}
