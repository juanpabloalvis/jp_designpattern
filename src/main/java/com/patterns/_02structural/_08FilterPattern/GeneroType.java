package com.patterns._02structural._08FilterPattern;

public enum GeneroType {
	MASCULINO,
	FEMENINO
}
