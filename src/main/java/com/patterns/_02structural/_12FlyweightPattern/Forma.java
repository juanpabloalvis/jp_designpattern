package com.patterns._02structural._12FlyweightPattern;

// Paso 1. creamos la interface
public interface Forma {
	void dibujar();
}
