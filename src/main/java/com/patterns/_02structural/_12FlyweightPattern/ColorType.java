package com.patterns._02structural._12FlyweightPattern;

public enum ColorType {
	AZUL, 
	AMARILLO,
	ROJO,
	VERDE, 
	CAFE,
	NEGRO,
	BLANCO
}
