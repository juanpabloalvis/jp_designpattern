package com.patterns._02structural._12FlyweightPattern;

// Paso 2. Clases concretas implementando la misma interface.
public class Circulo implements Forma {

	private ColorType color;
	private int x;
	private int y;
	private int radio;

	public Circulo(ColorType color) {
		this.color = color;
	}
	
	@Override
	public void dibujar() {
		System.out.println("Circulo: Dibujar() [Color : " + color + ", x : " + x + ", y :" + y + ", radio :" + radio);
		
	}

	public ColorType getColor() {
		return color;
	}

	public void setColor(ColorType color) {
		this.color = color;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRadio() {
		return radio;
	}

	public void setRadio(int radio) {
		this.radio = radio;
	}

}
