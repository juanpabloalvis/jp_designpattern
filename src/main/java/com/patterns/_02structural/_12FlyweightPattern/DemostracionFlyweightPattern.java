package com.patterns._02structural._12FlyweightPattern;

import java.util.Random;

public class DemostracionFlyweightPattern {

	public static void main(String[] args) {
		
		for (int i = 0; i <20; i++) {
			Circulo circulo = (Circulo)FabricaForma.getCirulo(getColorAleatorio());
			circulo.setX(getXAleatorio());
			circulo.setY(getYAleatorio());
			circulo.setRadio(150);
			circulo.dibujar();
		}

	}

	private static int getXAleatorio() {
		 return (int)(Math.random()*100 );
	}

	private static int getYAleatorio() {
		 return (int)(Math.random()*100 );
	}

	private static ColorType getColorAleatorio() {
		int pick = new Random().nextInt(ColorType.values().length);
	    return ColorType.values()[pick];
	}

}
