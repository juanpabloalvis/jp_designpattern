package com.patterns._02structural._12FlyweightPattern;

import java.util.HashMap;

//Paso 3. Se crea una fabrica para generar obejtos de clase concreta basados en informacion dada.
public class FabricaForma {
	private static final HashMap<ColorType, Forma> mapaCirculo = new HashMap<>();
	
	public static Circulo getCirulo(ColorType circuloByKey){
		Circulo circulo = (Circulo)mapaCirculo.get(circuloByKey);
		if(circulo == null){
			circulo = new Circulo(circuloByKey);
			mapaCirculo.put(circuloByKey, circulo);
			System.out.println("Creando circulos de color: "+circuloByKey);
		}
		return circulo;
	}

}
