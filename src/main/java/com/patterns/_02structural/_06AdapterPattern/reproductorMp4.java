package com.patterns._02structural._06AdapterPattern;

// Paso 2. Creamos las clases concretas implementando las interfaces.

public class reproductorMp4 implements ReproductorMultimediaAvanzado{

	@Override
	public void reproducirVlc(String nombreArchivo) {
		
	}

	@Override
	public void reproducirMp4(String nombreArchivo) {
		System.out.println("Ejecutando archivo mp4, NombreArchivo: "+nombreArchivo);
	}

}
