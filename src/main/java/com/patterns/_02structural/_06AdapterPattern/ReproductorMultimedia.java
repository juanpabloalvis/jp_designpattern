package com.patterns._02structural._06AdapterPattern;

// Paso 1. Crear interfaces para reproductor multimedia y reproductor multimedia avanzazado 
public interface ReproductorMultimedia {
	public void reproducir(TipoAudio tipoAudio, String nombreArchivo);
}
