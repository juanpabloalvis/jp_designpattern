package com.patterns._02structural._06AdapterPattern;

//Paso 4. Clase concreta implementando la interface "ReproductorMultimedia".
public class AudioReproductor  implements ReproductorMultimedia{

	AdaptadorReproductor adaptadorReproductor; 
	@Override
	public void reproducir(TipoAudio tipoAudio, String nombreArchivo) {
		// Soporte incorporado para reproducir archivos de musica mp3.
		if(tipoAudio.equals(TipoAudio.MP3)){
	         System.out.println("Reproduciendo archivo mp3. Nombre: " + nombreArchivo);			
	      }
		//
		else if(tipoAudio.equals(TipoAudio.VLC)||tipoAudio.equals(TipoAudio.MP4)){
			adaptadorReproductor= new AdaptadorReproductor(tipoAudio);
			adaptadorReproductor.reproducir(tipoAudio, nombreArchivo);
		}else{
			System.out.println("Tipo de medio inválido. "+tipoAudio+" formato no soportado.");
		}
	}

}
