package com.patterns._02structural._06AdapterPattern;

// Paso 1. Crear interfaces para reproductor multimedia y reproductor multimedia avanzazado 
public interface ReproductorMultimediaAvanzado {
	public void reproducirVlc(String nombreArchivo);
	public void reproducirMp4(String nombreArchivo);	
}
