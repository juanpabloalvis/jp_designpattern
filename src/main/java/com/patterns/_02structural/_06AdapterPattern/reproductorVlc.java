package com.patterns._02structural._06AdapterPattern;

// Paso 2. Creamos las clases concretas implementando las interfaces.

public class reproductorVlc implements ReproductorMultimediaAvanzado{

	@Override
	public void reproducirVlc(String nombreArchivo) {
		System.out.println("Ejecutando archivo vlc, NombreArchivo: "+nombreArchivo);
	}

	@Override
	public void reproducirMp4(String nombreArchivo) {
		// TODO Auto-generated method stub
		
	}

}
