package com.patterns._02structural._06AdapterPattern;

public enum TipoAudio {
	MP3, MP4, VLC, AVI
}
