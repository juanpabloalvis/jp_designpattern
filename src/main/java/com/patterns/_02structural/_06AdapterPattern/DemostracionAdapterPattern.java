package com.patterns._02structural._06AdapterPattern;

public class DemostracionAdapterPattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AudioReproductor audioReproductor = new AudioReproductor();
		audioReproductor.reproducir(TipoAudio.MP3, "mas allá del horizonte.mp3");
		audioReproductor.reproducir(TipoAudio.MP4, "solo.mp4");
		audioReproductor.reproducir(TipoAudio.VLC, "9 sonatha.vlc");
		audioReproductor.reproducir(TipoAudio.AVI, "Boulevard of broken dreams.avi");

	}

}
