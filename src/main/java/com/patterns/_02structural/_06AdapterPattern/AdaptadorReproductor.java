package com.patterns._02structural._06AdapterPattern;

// Paso 3. Creamos clase adaptador implementando la interface ReproductorMultimedia
public class AdaptadorReproductor implements ReproductorMultimedia{

	ReproductorMultimediaAvanzado reproductorMultimediaAvanzado;
	
	public AdaptadorReproductor(TipoAudio tipoAudio) {
		if(tipoAudio.equals(TipoAudio.VLC)){
			reproductorMultimediaAvanzado = new reproductorVlc();
		}else if(tipoAudio.equals(TipoAudio.MP4)){
			reproductorMultimediaAvanzado = new reproductorMp4();
		}
		
	}

	@Override
	public void reproducir(TipoAudio tipoAudio, String nombreArchivo) {
		if(tipoAudio.equals(TipoAudio.VLC)){
			reproductorMultimediaAvanzado.reproducirVlc(nombreArchivo);
		}else if(tipoAudio.equals(TipoAudio.MP4)){
			reproductorMultimediaAvanzado.reproducirMp4(nombreArchivo);
		}
		
	}

}
