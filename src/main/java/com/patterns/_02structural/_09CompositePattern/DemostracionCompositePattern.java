package com.patterns._02structural._09CompositePattern;

import java.math.BigDecimal;

public class DemostracionCompositePattern {
	public static void main(String args[]){
		Empleado CEO = new Empleado("Juan", "Compañia JP", new BigDecimal("2000.23"));
		Empleado directorArquitectura = new Empleado("El chavo del ocho", "Arquitectura de sistemas", new BigDecimal("2000.23"));
		Empleado directorPruebas = new Empleado("Kiko", "Director de pruebas", new BigDecimal("2000.23"));
		
		Empleado arquitecto1 = new Empleado("Leonardo", "Arquitectura de sistemas", new BigDecimal("1000.23"));
		Empleado arquitecto2 = new Empleado("Miguelangelo", "Arquitectura de sistemas", new BigDecimal("1000.23"));
		Empleado tester1 = new Empleado("Donatello", "Arquitectura de sistemas", new BigDecimal("1000.23"));
		Empleado tester2 = new Empleado("Raphael", "Arquitectura de sistemas", new BigDecimal("1000.23"));
		
		CEO.addSubordinado(directorArquitectura);
		CEO.addSubordinado(directorPruebas);

		directorArquitectura.addSubordinado(arquitecto1);
		directorArquitectura.addSubordinado(arquitecto2);

		directorPruebas.addSubordinado(tester1);
		directorPruebas.addSubordinado(tester2);
		
		System.out.println("Imprimir empleados de la organizacion: "+CEO);
		
		for (Empleado empleado: CEO.getSubordinados()) {
			System.out.println(empleado);
			for (Empleado subOrdinado: empleado.getSubordinados()) {
				System.out.println(subOrdinado);
			}
		}
	}
}
