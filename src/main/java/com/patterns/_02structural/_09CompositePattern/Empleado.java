package com.patterns._02structural._09CompositePattern;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// Paso 1. Un empleado tiene muchos empleados
public class Empleado {
	private String nombre;
	private String departamento;
	private BigDecimal salario;
	private List<Empleado> subordinados;
	
	
	public Empleado(String nombre, String departamento, BigDecimal salario) {
		super();
		this.nombre = nombre;
		this.departamento = departamento;
		this.salario = salario;
		this.subordinados = new ArrayList<Empleado>();
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public BigDecimal getSalario() {
		return salario;
	}
	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}
	public List<Empleado> getSubordinados() {
		return subordinados;
	}
	public void addSubordinado(Empleado subordinado) {
		this.subordinados.add(subordinado);
	}
	public void removeSubordinado(Empleado subordinado) {
		this.subordinados.remove(subordinado);
	}
	
	public String toString(){
	      return ("Empleado :[ Nombre : " + nombre + ", departamento : " + departamento+ ", salario :" + salario +" ]");
	   } 
}
