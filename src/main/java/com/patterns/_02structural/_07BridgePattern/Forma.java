package com.patterns._02structural._07BridgePattern;

// Paso 3. Crear una clase abstracta Forma utilizando la interface DibujarAPI
public abstract class Forma {
	protected DibujarAPI dibujarAPI;
	
	protected Forma(DibujarAPI dibujarAPI){
		this.dibujarAPI = dibujarAPI;
	}
	
	public abstract void dibujar();
}
