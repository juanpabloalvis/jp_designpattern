package com.patterns._02structural._07BridgePattern;

//Paso 2. Clase concreta bridge que implementa la interface DibujarAPI
public class CirculoRojo implements DibujarAPI {

	@Override
	public void dibujarCirculo(int radio, int x, int y) {
		System.out.println("Dibujando Circulo[ color: rojo, radio: " + radio + ", x: " + x + ", " + y + "]");
	}

}
