package com.patterns._02structural._07BridgePattern;

//Paso 1. Crear interface
public interface DibujarAPI {
	public void dibujarCirculo(int radio, int x, int y);
}
