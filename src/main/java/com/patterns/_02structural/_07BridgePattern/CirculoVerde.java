package com.patterns._02structural._07BridgePattern;

//Paso 2. Clase concreta bridge que implementa la interface DibujarAPI
public class CirculoVerde implements DibujarAPI {

	@Override
	public void dibujarCirculo(int radio, int x, int y) {
		System.out.println("Dibujando Circulo[ color: verde, radius: " + radio + ", x: " + x + ", " + y + "]");
	}

}
