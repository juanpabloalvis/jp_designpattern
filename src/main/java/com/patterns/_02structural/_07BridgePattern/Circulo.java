package com.patterns._02structural._07BridgePattern;

//Paso 4. Clase concreta que implementa la interface Forma
public class Circulo extends Forma {
	private int x, y, radio;

	protected Circulo(int x, int y, int radio, DibujarAPI dibujarAPI) {
		super(dibujarAPI);
		this.x = x;
		this.y = y;
		this.radio = radio;
	}

	@Override
	public void dibujar() {
		dibujarAPI.dibujarCirculo(radio, x, y);

	}

}
