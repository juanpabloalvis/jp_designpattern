package com.patterns._02structural._07BridgePattern;

public class DemostracionBridgePattern {

	public static void main(String[] args) {
		Forma circuloRojo = new Circulo(100, 100, 15, new CirculoRojo());
		Forma circuloVerde = new Circulo(100, 100, 15, new CirculoVerde());

		circuloRojo.dibujar();
		circuloVerde.dibujar();

	}

}
